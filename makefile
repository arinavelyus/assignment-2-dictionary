ASM = nasm 
ASMFLAGS = -f elf64
LD = ld

.PHONY: clean

program: main.o dict.o lib.o
	$(LD) -o $@ $^
lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o lib.o $^
dict.o: dict.asm lib.o 
	$(ASM) $(ASMFLAGS) -o dict.o $<
main.o: main.asm dict.o words.inc
	$(ASM) $(ASMFLAGS) -o main.o $<

clean:
	$(RM) *.o main
